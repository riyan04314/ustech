<?php

namespace App\Http\Requests;

use App\Internalmember;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateInternalmemberRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('internalmember_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'emp_category'             => [
                'required',
            ],
            'first_name'               => [
                'max:255',
                'required',
            ],
            'last_name'                => [
                'max:255',
            ],
            'institution_name'         => [
                'max:255',
            ],
            'contact_no'               => [
                'max:20',
            ],
            'certificate_approve_by'   => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'certificate_approve_date' => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'default_certificate'      => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'created_by'               => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
