<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class MatchDetail extends Model
{
    use SoftDeletes;

    public $table = 'match_details';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'match',
        'team',
        'player',
        'run',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
	

    public function playersDetails()
    {
    	return $this->belongsTo('App\Player','player');
    }
		
	public function matchDetails()
    {
    	return $this->belongsTo('App\Match','match');
    }
		
	public function teamDetails()
    {
    	return $this->belongsTo('App\Team','team');
    }
		
	
}
