<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Match extends Model
{
    use SoftDeletes;

    public $table = 'matches';

    protected $dates = [
        'match_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'team_one',
        'team_two',
        'winner',
        'match_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getMatchDateAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setMatchDateAttribute($value)
    {
        $this->attributes['match_date'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }
	
    public function teamsOne()
    {
    	return $this->belongsTo('App\Team','team_one');
    }	
		
    public function teamsTwo()
    {
    	return $this->belongsTo('App\Team','team_two');
    }	
	    
	public function winnerTeam()
    {
    	return $this->belongsTo('App\Team','winner');
    }	
		
	
}
