@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.match_details') }} on {{date('d-m-Y H:i',strtotime($match[0]->match_date))}}
    </div>

    <div class="card-body">
		@if ($message = Session::get('success'))
			<div class="alert alert-success">
				<p>{{ $message }}</p>
			</div>
		@endif
		@if ($message = Session::get('error'))
			<div class="alert alert-danger">
				<p>{{ $message }}</p>
			</div>
		@endif			
		
        <form method="POST" action="{{ route("admin.matches.details.update", [$match[0]->id]) }}" enctype="multipart/form-data">
            @method('POST')
            @csrf
			<input type="hidden" name="type" value="{{count($match_details)}}" />
			<div class="row">	
				<?php 
					$j = 0;
					$k = -1;
					while($j<2){ 
						$j++;
						$team_id = 0;
						if($j == 1){
							$team_id = $match[0]->team_one;
						}
						elseif($j == 2){
							$team_id = $match[0]->team_two;
						}
				?> 
				<div class="col-lg-6 col-md-6">	
					
						<div class="card-header">
							@if($j == 1){{ $teamsOne }}@else{{ $teamsTwo }}@endif
						</div>
					<div  style="border:1px solid #b9883b; padding:10px; margin-bottom:10px;">	
			<?php 
				$i = 0;
				while($i<config('data.playerno'))
				{
					$i++;
					$k++;
			?>
			
			@if(isset($match_details[$k]['id']))
				@php($match_details_id = $match_details[$k]['id'])
			@else
				@php($match_details_id = 0)
			@endif
			<div class="row">			
				<div class="col-md-6">			
					<div class="form-group">
						<input type="hidden" name="matchDetails[{{$k}}][id]" value="{{$match_details_id}}" />
						<input type="hidden" name="matchDetails[{{$k}}][team]" value="{{$team_id}}" />
						<select name="matchDetails[{{$k}}][player]" id="team_one" class="form-control">
							@foreach($playerlist as $val)
								<option value="{{$val['id']}}" @if(isset($match_details[$k]['player']) && $match_details[$k]['player'] ==  $val['id']) selected @endif >{{$val['first_name'].' '.$val['last_name']}}</option>
							@endforeach
						</select>                
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" name="matchDetails[{{$k}}][run]" value="@if(isset($match_details[$k]['run'])){{$match_details[$k]['run']}}@else{{0}}@endif" class="form-control">               
					</div>
				</div>				
			</div>
			<?php } ?>

					</div>
				</div>
				<?php } ?>
				
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<button class="btn btn-danger" type="submit">
							{{ trans('global.save') }}
						</button>

					</div>
				</div>
			</div>
		</form>	

			<div class="clear" ></div>

        
    </div>
</div>



@endsection