

		<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="mt-3">
                            <a href="{{ route("admin.home") }}" class="btn create-btn text-white no-wrap d-flex align-items-center">
                                <i data-feather="plus"></i>
                                <span class="hide-menu ml-2 text-uppercase">{{ trans('global.dashboard') }}</span>
                            </a>
                        </li>
						@can('user_management_access')
							<li class="sidebar-item selected">
								<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
									aria-expanded="false">
									<i data-feather="home" class="mr-2"></i>
									<span class="hide-menu">{{ trans('cruds.userManagement.title') }}</span>
								</a>
								<ul aria-expanded="false" class="collapse  first-level">
									
									@can('permission_access')
										<li class="sidebar-item">
											<a href="{{ route("admin.permissions.index") }}" class="sidebar-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
												<i data-feather="droplet" class="mr-2"></i>
												<span class="hide-menu">{{ trans('cruds.permission.title') }}</span>
											</a>
										</li>
									@endcan
									@can('role_access')
										<li class="sidebar-item">
											<a href="{{ route("admin.roles.index") }}" class="sidebar-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
												<i data-feather="droplet" class="mr-2"></i>
												<span class="hide-menu">{{ trans('cruds.role.title') }}</span>
											</a>
										</li>
									@endcan
									@can('user_access')
										<li class="sidebar-item">
											<a href="{{ route("admin.users.index") }}" class="sidebar-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
												<i data-feather="droplet" class="mr-2"></i>
												<span class="hide-menu">{{ trans('cruds.user.title') }}</span>
											</a>
										</li>
									@endcan	
								</ul>
							</li>
						@endcan

						@can('team_access')
							<li class="sidebar-item selected">
								<a href="{{ route("admin.teams.index") }}" class="sidebar-link {{ request()->is('admin/teams') || request()->is('admin/teams/*') ? 'active' : '' }}">
									<i data-feather="home" class="mr-2"></i>
									{{ trans('cruds.team.title') }}
								</a>
							</li>
						@endcan
						@can('player_access')
							<li class="sidebar-item selected">
								<a href="{{ route("admin.players.index") }}" class="sidebar-link {{ request()->is('admin/players') || request()->is('admin/players/*') ? 'active' : '' }}">
									<i data-feather="home" class="mr-2"></i>
									{{ trans('cruds.player.title') }}
								</a>
							</li>
						@endcan
						@can('match_access')
							<li class="sidebar-item selected">
								<a href="{{ route("admin.matches.index") }}" class="sidebar-link {{ request()->is('admin/matches') || request()->is('admin/matches/*') ? 'active' : '' }}">
									<i data-feather="home" class="mr-2"></i>
									{{ trans('cruds.match.title') }}
								</a>
							</li>
						@endcan
						@if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
							@can('profile_password_edit')
								<li class="sidebar-item selected">
									<a class="sidebar-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
										<i data-feather="home" class="mr-2"></i>
										{{ trans('global.change_password') }}
									</a>
								</li>
							@endcan
						@endif
						<li class="sidebar-item selected">
							<a href="#" class="sidebar-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
								<i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

								</i>
								{{ trans('global.logout') }}
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</aside>


