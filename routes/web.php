<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Teams
    Route::delete('teams/destroy', 'TeamController@massDestroy')->name('teams.massDestroy');
    Route::post('teams/media', 'TeamController@storeMedia')->name('teams.storeMedia');
    Route::post('teams/ckmedia', 'TeamController@storeCKEditorImages')->name('teams.storeCKEditorImages');
    Route::resource('teams', 'TeamController');

    // Players
    Route::get('players/matchDetails/{playerid}', 'PlayersController@matchDetails')->name('players.matchDetails');
    Route::delete('players/destroy', 'PlayersController@massDestroy')->name('players.massDestroy');
    Route::post('players/media', 'PlayersController@storeMedia')->name('players.storeMedia');
    Route::post('players/ckmedia', 'PlayersController@storeCKEditorImages')->name('players.storeCKEditorImages');
    Route::resource('players', 'PlayersController');

    // Matches
    Route::get('matches/details/{matchid}', 'MatchController@details')->name('matches.details');
	Route::post('matches/details/update/{matchid}', 'MatchController@detailsUpdate')->name('matches.details.update');
	Route::delete('matches/destroy', 'MatchController@massDestroy')->name('matches.massDestroy');
    Route::resource('matches', 'MatchController');

    // Player Details
    Route::delete('player-details/destroy', 'PlayerDetailsController@massDestroy')->name('player-details.massDestroy');
    Route::resource('player-details', 'PlayerDetailsController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
