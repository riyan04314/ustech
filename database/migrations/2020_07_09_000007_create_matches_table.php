<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_one');
            $table->integer('team_two');
            $table->integer('winner');
            $table->datetime('match_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
