@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.playerDetail.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.player-details.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.playerDetail.fields.id') }}
                        </th>
                        <td>
                            {{ $playerDetail->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.playerDetail.fields.player') }}
                        </th>
                        <td>
                            {{ $playerDetail->player }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.playerDetail.fields.match') }}
                        </th>
                        <td>
                            {{ $playerDetail->match }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.playerDetail.fields.team') }}
                        </th>
                        <td>
                            {{ $playerDetail->team }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.playerDetail.fields.run') }}
                        </th>
                        <td>
                            {{ $playerDetail->run }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.player-details.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection